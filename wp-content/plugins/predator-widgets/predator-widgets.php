<?php
/**
* Plugin Name: Predator widgets
* Description: Adds widgets to predator site
* Author: Jovan Dosen
* Version: 0.0.1
* License: GPL2
*/

if(!defined('ABSPATH')){
	exit();
}

require __DIR__ . '/vendor/autoload.php';

use Predator\Widgets\FooWidget;

class PredatorWidgets
{
	public function __construct()
	{
		add_action('plugins_loaded', array($this, 'boot'));
	}

	public function boot()
	{
		$foo_widget = new FooWidget();
	}

	public static function create()
	{
		return new self();
	}
}

PredatorWidgets::create();

?>