<?php

namespace Event;

class EventData
{
	public function __construct()
	{
		add_action('init', array($this, 'addEventLogic'));
	}

	public function addEventLogic()
	{
		$labels = array(
	        'name'                => _x( 'Events', 'Post Type General Name', 'development' ),
	        'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'development' ),
	        'menu_name'           => __( 'Events', 'development' ),
	        'parent_item_colon'   => __( 'Parent Event', 'development' ),
	        'all_items'           => __( 'All Events', 'development' ),
	        'view_item'           => __( 'View Event', 'development' ),
	        'add_new_item'        => __( 'Add New Event', 'development' ),
	        'add_new'             => __( 'Add New', 'development' ),
	        'edit_item'           => __( 'Edit Event', 'development' ),
	        'update_item'         => __( 'Update Event', 'development' ),
	        'search_items'        => __( 'Search Event', 'development' ),
	        'not_found'           => __( 'Not Found', 'development' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'development' ),
	    );

	    $args = array(
	        'label'               => __( 'events', 'development' ),
	        'description'         => __( 'Event news and reviews', 'development' ),
	        'labels'              => $labels,
	        'supports'            => array( 'title', 'editor', 'thumbnail', ),
	        'taxonomies'          => array( 'genres' ),
	        'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_nav_menus'   => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 5,
	        'can_export'          => true,
	        'has_archive'         => true,
	        'exclude_from_search' => false,
	        'publicly_queryable'  => true,
	        'capability_type'     => 'page',
	    );
	    
    	register_post_type( 'events', $args );
	}
}