<?php

namespace Event;

class EventCategory
{
	public function __construct()
	{
		add_action('init', array($this, 'addCategoryLogic'));
	}

	public function addCategoryLogic()
	{
		$labels = array(
	        'name'          => 'Cities',
	        'singular_name' => 'City',
	        'edit_item'     => 'Edit City',
	        'update_item'   => 'Update City',
	        'add_new_item'  => 'Add New City',
	        'menu_name'     => 'Cities'
    	);

	    $args = array(
	        'hierarchical'      => true,
	        'labels'            => $labels,
	        'show_ui'           => true,
	        'show_admin_column' => true,
	        'rewrite'           => array( 'slug' => 'city' )
	    );

    	register_taxonomy( 'city', 'events', $args );
	}
}