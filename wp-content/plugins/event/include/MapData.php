<?php

namespace Event;

class MapData
{
	public function __construct()
	{
		add_action('add_meta_boxes', array($this, 'addMapLogic'));
		add_action('save_post', array($this, 'saveMapData'));
	}

	public function addMapLogic()
	{
		$screens = ['events'];

	    foreach ($screens as $screen) {
	        add_meta_box(
	            'box_id',           
	            'Google map',  
	            array($this, 'addMapHtml'),  
	            $screen                   
	        );
	    }
	}

	public function addMapHtml($post)
	{
		$value = get_post_meta($post->ID, 'box_id', true);

		if(!empty($value) && isset($value)){
			$results = explode(" ", $value);
			$latitude = $results[0];
			$longitude = $results[1];
		} else {
			$latitude = '';
			$longitude = '';
		}

	    ?>
	    <div id="googleMap"></div>
	    <input type="hidden" name="data" value="" id="positionElement" data-latitude="<?php echo $latitude; ?>" data-longitude="<?php echo $longitude; ?>" />
	    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCon0ixshiNHYtcSa8DLpXRCPx0sJ-NcxM"></script>
	    <?php
	}

	public function saveMapData($post_id)
	{
		if (array_key_exists('data', $_POST)) {
	        update_post_meta(
	            $post_id,
	            'box_id',
	            $_POST['data']
	        );
	    }
	}
}