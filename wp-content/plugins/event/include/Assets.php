<?php

namespace Event;

class Assets
{
	public function __construct()
	{
		// add_action('wp_enqueue_scripts', array($this, 'addAssets')); // add js and css on front pages
		add_action('admin_enqueue_scripts', array($this, 'addAssets')); // add js and css on admin pages
	}

	public function addAssets()
	{
		wp_register_script('appjs', plugins_url('/event/assets/js/app.js'), array('jquery'),'1.1', true);
		wp_enqueue_script('appjs');

		wp_register_style('appcss', plugins_url('/event/assets/css/app.css'));
		wp_enqueue_style('appcss');
	}
}