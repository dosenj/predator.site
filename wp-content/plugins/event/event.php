<?php
/**
* Plugin Name: Event
* Plugin URI: http://predator.site/event
* Description: This is event plugin. You can use it to create events.
* Version: 1.0.0
* Author: Jovan Dosen
* Author URI: http://predator.site/jovan-dosen
* License: GPL2

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

if(!defined('ABSPATH')){
	exit();
}

require __DIR__ . '/vendor/autoload.php'; // adds namespace class logic

use Event\EventData; // adds EventData custom post type
use Event\EventCategory; // adds EventCategory taxonomy
use Event\MapData; // adds Custom field - google map
use Event\Assets; // register js and css

if(!class_exists('Event')){
	class Event
	{
		public function __construct()
		{
			add_action('plugins_loaded', array($this, 'boot')); // first hook to run
		}

		public function boot()
		{
			$eventData = new EventData(); // creates custom post type
			$eventCategory = new EventCategory(); // creates taxonomy
			$mapData = new MapData(); // creates google map as custom field for events 
			$assets = new Assets(); // includes css and js logic
		}

		public static function create()
		{
			return new self(); // creates self instance, well and good
		}
	}
}

Event::create(); // run plugin
