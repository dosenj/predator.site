function addMapToEvents()
{
	var hiddenData = document.getElementById("positionElement");

	var la = hiddenData.dataset.latitude;
	var lo = hiddenData.dataset.longitude;

	la = parseFloat(la);
	lo = parseFloat(lo);

	var positionData;
	var googleMap;
	var marker;

	if(la == '' || lo == '' || isNaN(la) || isNaN(lo)){
		positionData = {lat: 48.8566, lng: 2.3522};
	} else {
		positionData = {lat: la, lng: lo};
	}

	googleMap = new google.maps.Map(document.getElementById('googleMap'), {
		center: positionData,
		zoom: 8
	});

	marker = new google.maps.Marker({position: positionData, map: googleMap, draggable: true});

	google.maps.event.addListener(marker, "click", function(event){
		var latitude = event.latLng.lat();
        var longitude = event.latLng.lng();
        document.getElementById("positionElement").value = latitude+' '+longitude;
	});
}

addMapToEvents();