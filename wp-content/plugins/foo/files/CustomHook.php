<?php

class CustomHook
{
	public function __construct()
	{
		add_action( 'admin_menu', array($this, 'wporg_options_page') );
		add_action('admin_menu', array($this, 'wporg_options_page_data'));
		add_action('admin_menu', array($this, 'addSubPage'));
	}

	public function wporg_options_page() 
	{
	    add_menu_page(
	        'Aurora title name',
	        'Aurora Options',
	        'manage_options',
	        'aurora',
	        array($this, 'addSomeHtml'),
	        'none',
	        20
	    );
	}

	public function addSomeHtml()
	{
		?>
		<div>
			<h4>This is demo app</h4>
			<p>Development...</p>
		</div>
		<?php
	}

	public function wporg_options_page_data()
	{
	    add_submenu_page(
	        'tools.php',
	        'WPOrg Options',
	        'WPOrg Options',
	        'manage_options',
	        'wporg',
	        array($this, 'showSubmenuHtml')
	    );
	}

	public function showSubmenuHtml()
	{
		?>
		<div>
			<h4>This is demo app</h4>
			<p>Development...</p>
			<p>Submenu content</p>
		</div>
		<?php
	}

	public function addSubPage()
	{
		add_submenu_page(
	        'edit.php?post_type=events',
	        'WPOrg Options',
	        'WPOrg Options',
	        'manage_options',
	        'wporg',
	        array($this, 'addHtmlToEvents')
	    );
	}

	public function addHtmlToEvents()
	{
		?>
		<div>
			<h4>Foo title</h4>
			<p>Foo content</p>
		</div>
		<?php
	}
}