<?php

class ShortcodeExample
{
	public function __construct()
	{
		add_action('init', array($this, 'addContent'));
	}

	public function addContent()
	{
		add_shortcode('content', array($this, 'contentData'));
	}

	public function contentData()
	{
		echo "Bla bla bla eee asasfqweffwewefgwe";
	}
}