<?php

class AddUser
{
	public function __construct()
	{
		// $this->create();
		// $this->update();
		// $this->delete();
		// $this->addUserMetaData();
		// $this->updateUserMetaData();
		// $this->deleteUserMetaData();
	}

	public function create()
	{
		$username = 'Patrick';
		$password = 'Bateman';
		$email = 'patrick@gmail.com';

		$user_data = [
			'user_login' => $username,
			'user_pass' => $password,
			'user_email' => $email
		];

		$user_id = wp_insert_user($user_data);

		if(!is_wp_error($user_id)){
			echo "User created:".$user_id;
		}
	}

	public function update()
	{
		$user_id = 4;

		$email = 'patrickbateman@gmail.com';

		$user = wp_update_user([
			'ID' => $user_id,
			'user_email' => $email
		]);

		if(is_wp_error($user)){
			// error
		} else {
			echo "User updated";
		}
	}

	public function delete()
	{
		wp_delete_user(3);
	}

	public function addUserMetaData()
	{
		add_user_meta(4, 'foo', 'FOO');
	}

	public function updateUserMetaData()
	{
		update_user_meta(4, 'foo', 'FOO AAAAAAAAAAAA');
	}

	public function deleteUserMetaData()
	{
		delete_user_meta(4, 'foo');
	}
}