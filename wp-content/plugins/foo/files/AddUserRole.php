<?php

class AddUserRole
{
	public function __construct()
	{
		// add_action('init', array($this, 'addRole'));
		// add_action('init', array($this, 'removeRole'));
	}

	public function addRole()
	{
		add_role(
	        'simple_role',
	        'Simple Role',
	        [
	            'read'         => true,
	            'edit_posts'   => true,
	            'upload_files' => true,
	        ]
    	);
	}

	public function removeRole()
	{
		remove_role('simple_role');
	}
}