<?php

class AddAssetsFiles
{
	public function __construct()
	{
		add_action('admin_enqueue_scripts', array($this, 'assetsFiles'));
	}

	public function assetsFiles()
	{
		wp_register_script('fooappjs', plugins_url('/foo/assets/js/fooapp.js'), array('jquery'),'1.1', true);
		wp_enqueue_script('fooappjs');

		wp_register_style('fooappcss', plugins_url('/foo/assets/css/fooapp.css'));
		wp_enqueue_style('fooappcss');
	}
}