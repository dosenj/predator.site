<?php

class DisplayEvents
{
	public function __construct()
	{
		add_action('pre_get_posts', array($this, 'showEventsData'));
	}

	public function showEventsData($query)
	{
		if (is_home() && $query->is_main_query()) {
        	$query->set('post_type', ['post', 'events']);
    	}
    	return $query;
	}
}