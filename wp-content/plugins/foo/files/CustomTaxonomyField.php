<?php

class CustomTaxonomyField
{
	public function __construct()
	{
		add_action('city_add_form_fields', array($this, 'addFieldLogic'));
		add_action('city_edit_form_fields', array($this, 'addFieldLogic'));
		add_action('created_city', array($this, 'saveData'));
		add_action('edited_city', array($this, 'saveData'));
	}

	public function addFieldLogic($term)
	{
		$value = get_term_meta($term->term_id, 'example', true);
		?>
		<input type="text" name="example" placeholder="Enter data..." id="taxo-field" value="<?php echo $value; ?>" />
		<?php
	}

	public function saveData($term_id)
	{
		if(isset($_POST['example'])){
			update_term_meta($term_id, 'example', $_POST['example']);
		}
	}
}