<?php
/**
* Plugin Name: Foo
* Description: Foo plugin description text
* Author: Jovan Dosen
* Version: 1.0.0
* License: GPL2
*/

if(!defined('ABSPATH')){
	exit();
}

require __DIR__ . '/files/CustomHook.php';
require __DIR__ . '/files/ShortcodeExample.php';
require __DIR__ . '/files/CustomTaxonomyField.php';
require __DIR__ . '/files/AddAssetsFiles.php';
require __DIR__ . '/files/DisplayEvents.php';
require __DIR__ . '/files/AddUser.php';
require __DIR__ . '/files/AddUserRole.php';

if(!class_exists('FooData')){
	class FooData
	{
		public function __construct()
		{
			add_action('plugins_loaded', array($this, 'appData'));
		}

		public function appData()
		{
			$customHook = new CustomHook();
			$shortcode = new ShortcodeExample();
			$customTaxonomyFiled = new CustomTaxonomyField();
			$assets = new AddAssetsFiles();
			$events = new DisplayEvents();
			$user = new AddUser();
			$role = new AddUserRole();
		}

		public static function create()
		{
			return new self();
		}
	}
}

FooData::create();