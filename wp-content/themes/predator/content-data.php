<?php

	if(have_posts()){
		while(have_posts()){
			the_post(); // get the post/page data
			the_title(); // page title
			the_content(); // page content
		}
	} else {
		echo "No page data found";
	}

?>