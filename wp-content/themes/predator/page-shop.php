<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Shop</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>

		<?php get_header(); ?>

		<div class="container">
			<h1>Shop page data:</h1>
			<?php get_template_part('content', 'data'); ?>
		</div>

		<?php wp_footer(); ?>
	</body>
</html>