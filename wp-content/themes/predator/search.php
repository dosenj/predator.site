<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Search results</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>

		<?php get_header(); ?>

		<div class="container">
			<?php
				$search_data = get_search_query();
				echo 'Search result: ' . $search_data;
			?>
		</div>

		<?php get_footer(); ?>

		<?php wp_footer(); ?>
	</body>
</html>