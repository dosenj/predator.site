<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Events</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>
		<?php get_header(); ?>

		<h1>Event data:</h1>

		<div class="container">
			<?php get_template_part('content', 'eve'); ?>
		</div>

		<?php wp_footer(); ?>
	</body>
</html>