<?php

function addAssetsContent()
{
	wp_enqueue_style('predatorcss', get_template_directory_uri() . '/css/predator.css', false, '1.1', 'all');
	wp_enqueue_script('predatorjs', get_template_directory_uri() . '/js/predator.js', array('jquery'), 1.1, true);
}

add_action('wp_enqueue_scripts', 'addAssetsContent');

function registerNavigationMenu()
{
	register_nav_menus(
	    array(
	    	'header-menu' => __('Header Menu')
	    )
    );
}

add_action('init', 'registerNavigationMenu');

add_theme_support( 'post-thumbnails' );

function my_menu() 
{
    add_options_page(
        'My Options',
        'My Menu',
        'manage_options',
        'my-unique-identifier',
        'my_options'
    );
}

add_action('admin_menu', 'my_menu' );

function my_options() 
{
    if ( !current_user_can( 'manage_options' ) ) {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    echo 'Here is where I output the HTML for my screen.';
    echo '</div><pre>';
}

function register_my_theme_settings_menu() 
{
    add_menu_page(
        "My Theme's Settings",
        "My Theme",
        "manage_options",
        "my-theme-settings-menu"
    );
}

function register_my_theme_more_settings_menu() 
{
    add_submenu_page(
        "my-themes-settings-menu",
        "More Settings for My Theme",
        "More Settings",
        "manage_options",
        "my-theme-more-settings-menu"
    );
}

add_action( "admin_menu", "register_my_theme_settings_menu");
add_action( "admin_menu", "register_my_theme_more_settings_menu");

function mt_add_pages() 
{
// Add a new submenu under Settings:
add_options_page(__('Test Settings','menu-test'), __('Test Settings','menu-test'), 'manage_options', 'testsettings', 'mt_settings_page');
 
// Add a new submenu under Tools:
add_management_page( __('Test Tools','menu-test'), __('Test Tools','menu-test'), 'manage_options', 'testtools', 'mt_tools_page');
 
// Add a new top-level menu (ill-advised):
add_menu_page(__('Test Toplevel','menu-test'), __('Test Top-level','menu-test'), 'manage_options', 'mt-top-level-handle', 'mt_toplevel_page' );
 
// Add a submenu to the custom top-level menu:
add_submenu_page('mt-top-level-handle', __('Test Sub-Level','menu-test'), __('Test Sub-Level','menu-test'), 'manage_options', 'sub-page', 'mt_sublevel_page');
 
// Add a second submenu to the custom top-level menu:
add_submenu_page('mt-top-level-handle', __('Test Sub-Level 2','menu-test'), __('Test Sub-Level 2','menu-test'), 'manage_options', 'sub-page2', 'mt_sublevel_page2');
}
 
// mt_settings_page() displays the page content for the Test settings sub-menu
function mt_settings_page() 
{
    echo "</pre>
    <h2>" . __( 'Test Settings', 'menu-test' ) . "</h2>
    <pre>
    ";
}
 
// mt_tools_page() displays the page content for the Test Tools sub-menu
function mt_tools_page() 
{
    echo "</pre>
    <h2>" . __( 'Test Tools', 'menu-test' ) . "</h2>
    <pre>
    ";
}
 
// mt_toplevel_page() displays the page content for the custom Test Top-Level menu
function mt_toplevel_page() 
{
    echo "</pre>
    <h2>" . __( 'Test Top-Level', 'menu-test' ) . "</h2>
    <pre>
    ";
}
 
// mt_sublevel_page() displays the page content for the first sub-menu
// of the custom Test Toplevel menu
function mt_sublevel_page() 
{
    echo "</pre>
    <h2>" . __( 'Test Sub-Level', 'menu-test' ) . "</h2>
    <pre>
";
}
 
// mt_sublevel_page2() displays the page content for the second sub-menu
// of the custom Test Top-Level menu
function mt_sublevel_page2() 
{
    echo "</pre>
    <h2>" . __( 'Test Sub-Level 2', 'menu-test' ) . "</h2>
    <pre>
";
}

add_action('admin_menu', 'mt_add_pages');

function themename_custom_header_setup() 
{
    $args = array(
        'default-image'      => get_template_directory_uri() . 'img/default-image.jpg',
        'default-text-color' => '000',
        'width'              => 1000,
        'height'             => 250,
        'flex-width'         => true,
        'flex-height'        => true,
    );
}

add_theme_support('custom-header');

add_action('after_setup_theme', 'themename_custom_header_setup' );

add_theme_support( 'custom-logo' );

function themename_custom_logo_setup() 
{
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
}

add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

function themename_widgets_init() 
{
    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'predator' ),
        'id'            => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Secondary Sidebar', 'predator' ),
        'id'            => 'sidebar-2',
        'before_widget' => '<ul><li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li></ul>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}

add_action( 'widgets_init', 'themename_widgets_init' );

function addCustomizerData($wp_customize)
{
    $wp_customize->add_setting('simple_input_text_field', array(
        'default' => 'Simple text to display'
    ));

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'simple_control_id',
        array(
            'label' => __('Simple label text', 'predator'),
            'section' => 'simple_section',
            'settings' => 'simple_input_text_field'
        )
    ));

    $wp_customize->add_section('simple_section', array(
        'title' => __('Simple section title'),
        'description' => __('Simple section description'),
        'panel' => 'simple_panel'
    ));

    $wp_customize->add_panel('simple_panel', array(
        'title' => __('Simple panel title'),
        'description' => __('Simple panel description'),
        'priority' => 160
    ));

    //

    $wp_customize->add_setting('image_upload');

    $wp_customize->add_control(new WP_Customize_Upload_Control(
        $wp_customize,
        'simple_image',
        array(
            'label' => __('Add some image'),
            'section' => 'simple_section',
            'settings' => 'image_upload'
        )
    ));

    // New area

    $wp_customize->add_setting('extra_text_field', array(
        'default' => 'Extra text field data'
    ));

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_text_field_control',
        array(
            'label' => __('Extra text field label', 'predator'),
            'section' => 'extra_section',
            'settings' => 'extra_text_field'
        )
    ));

    $wp_customize->add_section('extra_section', array(
        'title' => __('Extra section AREA'),
        'description' => __('Extra section description'),
        'panel' => 'extra_panel'
    ));

    $wp_customize->add_panel('extra_panel', array(
        'title' => __('Extra panel AREA'),
        'description' => __('Extra panel description'),
        'priority' => 200
    ));

    // 

    $wp_customize->add_setting('extra_image');

    $wp_customize->add_control(new WP_Customize_Upload_Control(
        $wp_customize,
        'extra_image_control',
        array(
            'label' => __('Extra image control'),
            'section' => 'extra_section',
            'settings' => 'extra_image'
        )
    ));

    //

    $wp_customize->add_setting('extra_second_image');

    $wp_customize->add_control(new WP_Customize_Image_Control(
        $wp_customize,
        'extra_second_image_control',
        array(
            'label' => __('Extra second image label'),
            'section' => 'extra_section',
            'settings' => 'extra_second_image'
        )
    ));

    //

    $wp_customize->add_setting('extra_color');

    $wp_customize->add_control(new WP_Customize_Color_Control(
        $wp_customize,
        'extra_color_control',
        array(
            'label' => __('Extra color label'),
            'section' => 'extra_section',
            'settings' => 'extra_color'
        )
    ));

    //

    $wp_customize->add_setting('extra_textarea');

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_textarea_control',
        array(
            'label' => __('Extra textarea label'),
            'section' => 'extra_section',
            'settings' => 'extra_textarea',
            'type' => 'textarea'
        )
    ));

    //

    $wp_customize->add_setting('extra_radio');

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_radio_control',
        array(
            'label' => __('Extra radio label'),
            'section' => 'extra_section',
            'settings' => 'extra_radio',
            'type' => 'radio',
            'choices' => array(
                'dark' => __('Dark'),
                'light' => __('Light')
            )
        )
    ));

    //

    $wp_customize->add_setting('extra_select');

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_select_control',
        array(
            'label' => __('Extra select label'),
            'section' => 'extra_section',
            'settings' => 'extra_select',
            'type' => 'select',
            'choices' => array(
                'foo' => __('Foo'),
                'bar' => __('Bar'),
                'baz' => __('Baz')
            )
        )
    ));

    //

    $wp_customize->add_setting('extra_checkbox');

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_checkbox_control',
        array(
            'label' => __('Extra checkbox label'),
            'section' => 'extra_section',
            'settings' => 'extra_checkbox',
            'type' => 'checkbox'
        )
    ));

    //

    $wp_customize->add_setting('extra_date');

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_date_control',
        array(
            'label' => __('Extra date label'),
            'section' => 'extra_section',
            'settings' => 'extra_date',
            'type' => 'date'
        )
    ));

    // end New area

}

add_action('customize_register', 'addCustomizerData');

?>