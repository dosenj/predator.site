<!DOCTYPE html>
<html lang="en">
	<head>
		<title>
			<?php echo get_bloginfo( 'name' ); ?>
		</title>
		<meta charset="utf-8">
		<meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>

		<?php get_header(); ?>

		<div class="container">
			<?php get_sidebar(); ?>
		</div>

		<div class="extra">
			<?php 

				get_template_part('extra'); 

				$music_file = get_template_directory_uri() . "/sounds/foo.mp3"; 
				echo do_shortcode('[audio mp3=' . $music_file . ']');

				$video_file = get_template_directory_uri() . "/videos/bar.mp4";
				echo do_shortcode('[video mp4=' . $video_file . ']');

				//

				$customizer_text_field = get_theme_mod('extra_text_field');

				echo $customizer_text_field;

				//

				$customizer_file_field = get_theme_mod('extra_image');

				?>
				<img src="<?php echo $customizer_file_field; ?>" width="200px" height="200px" />
				<?php

				//

				$customizer_image_field = get_theme_mod('extra_second_image');

				?>
				<img src="<?php echo $customizer_image_field; ?>" width="200px" height="200px" />
				<?php

				//

				$customizer_textarea_field = get_theme_mod('extra_textarea');

				echo $customizer_textarea_field;

				//

				$customizer_radio_field = get_theme_mod('extra_radio');

				echo $customizer_radio_field;

				//

				$customizer_select_field = get_theme_mod('extra_select');

				echo $customizer_select_field;

				//

				$customizer_checkbox_field = get_theme_mod('extra_checkbox');

				echo $customizer_checkbox_field;

				//

				$customizer_date_field = get_theme_mod('extra_date');

				echo $customizer_date_field;

			?>
		</div>

		<?php get_footer(); ?>

		<?php wp_footer(); ?>
	</body>
</html>