<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'predator');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'aurora');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iNA{OR374>T Lg}iq%^@)5o&ogDi-CsD6XZm>s+8#3{3ZYm&jIg_5)!?7UeV{vcD');
define('SECURE_AUTH_KEY',  '2T U?G[e)cW};HoqZ`+P<Be9Gp.$zd@cS; Ih+h6,{/LyLH{5MA}W!H>!j`t.Q8u');
define('LOGGED_IN_KEY',    'v}H`U)-uMtNgXz0<{GRzUIt6{e{{ABEMOjxKYVw3iIe&7Xo$R9_`xE7U83%Y@DU_');
define('NONCE_KEY',        '~01%~b*_:pjpc$7r7*}R$@V {QKJy7d!@Nib`^qe?EWL,3A<kyybpf~J|yj33YDs');
define('AUTH_SALT',        'G~DA.=[s9H|#C Pt}Ea4n*&4X>*c<]v).!1+9iyBg;S3rD4.>8r/OJx#bF4uCn?Q');
define('SECURE_AUTH_SALT', 'Cl,zXs#D~vW5Q%1~}@4II,pfx p5{TcUkPL)9hZ=n{ dpzk2&AKntI4$s;*IFB-f');
define('LOGGED_IN_SALT',   '~+(wivlb?S@r@~AwWSGpTDUn=r5jTE65J]p}E,E#6A/YjTWJBv/iHuNCl%g]Ir j');
define('NONCE_SALT',       'bV.5Mt0qSYx)*qVCj[dMG|igV`dnR$WP*VbTbM;t}1V*wxb;UQpIligKd?JCU}*=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

define('WP_DEBUG_LOG', true );

define('WP_DEBUG_DISPLAY', true );

define('SAVEQUERIES', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
